# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
#input = x coord and y coord
#test both x and y to be within range of (0,10) INCLUSIVE
    return x in range(11) and y in range(11)
#return true or false

print(is_inside_bounds(3, 11))
