# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
from statistics import mean

def calculate_average(values):
    return mean(values) if values else None

print(calculate_average([2, 2, 3]))
