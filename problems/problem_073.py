# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

from functools import reduce


class Student:
    def __init__(self, name):
        self.name = name
        self.score = []

    def add_score(self, score):
        self.score.append(score)

    def get_avg(self):
        if len(self.score) == 0:
            return None
        return reduce(lambda a, b: a +b, self.score) / len(self.score)


student = Student("Malik")

student.add_score(80)

print(student.get_avg())
