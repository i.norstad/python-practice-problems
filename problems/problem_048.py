# Write a function that meets these requirements.
#
# Name:       count_word_frequencies
# Parameters: sentence, a string
# Returns:    a dictionary whose keys are the words in the
#             sentence and their values are the number of
#             times that word has appeared in the sentence
#
# The sentence will contain now punctuation.
#
# This is "case sensitive". That means the word "Table" and "table"
# are considered different words.
#
# Examples:
#    * sentence: "I came I saw I learned"
#      result:   {"I": 3, "came": 1, "saw": 1, "learned": 1}
#    * sentence: "Hello Hello Hello"
#      result:   {"Hello": 3}

## FUNCTION PSEUDOCODE
# function count_word_frequencies(sentence):
    # words = split the sentence
    # counts = new empty dictionary
    # for each word in words
        # if the word is not in counts
            # counts[word] = 0
        # add one to counts[word]
    # return counts


# CAN YOU DO THIS WITH MAP???

def count_word_frequencies(sentence):
#    word_count = new empty dictionary
    word_count = {}
    # split_words = split the sentence into words
    split_words = sentence.split(' ')
    # print(split_words)
    # for each word in split_words:
    for word in split_words:
        # if the word is not in word_count:
        if word not in word_count:
            # word_count[word] = 0
            word_count[word] = 0
        #add 1 to word_count[word]
        word_count[word] += 1
    #return word_count
    return word_count


print(count_word_frequencies('apples are are cool Cool'))
