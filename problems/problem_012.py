# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
# test if num % 15 = 0 to handle number % 5 & number % 3 = 0
# if true return 'fizzbuzz'
    if number % 15 == 0:
        return 'fizzbuzz'
#else if num % 5 == 0 return 'buzz'
    elif number % 5 == 0:
        return 'buzz'
#else if num % 3 == 0 return 'fizz'
    elif number % 3 == 0:
        return 'fizz'
#else return num
    return number
print(fizzbuzz(19))
