# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    return None if not values else max(values)

print(max_in_list([13, 56, 3, 99]))
