# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
#define var for flour, eggs, oil
    items = set(['flour', 'eggs', 'oil'])
#check if all 3 items are in list
    set_ingredients = set(ingredients)
    return items == set_ingredients
#return true if true
#otherwise return false

print(can_make_pasta(['sugar', 'oil', 'eggs']))
