# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

from functools import reduce

def add_csv_lines(csv_lines):
    #instantiate new list
    new_list = []
    # loop through csv lines to separate ea str into a list
    for string in csv_lines:
        sep_strings = string.split(',')
        #print(sep_strings)
        sep_sum = 0
        #No worky: reduce:
        # x = reduce(lambda a, b: a + b, int(sep_strings), 0)
        # new_list.append(x)
        for val in sep_strings:
            sep_sum += int(val)
            new_list.append(sep_sum)
    return new_list
#return new list with each entry being the corresponding sum of nums in respective string:
#map?
print(add_csv_lines(['12, 32, 2', '3', '11, 7']))
