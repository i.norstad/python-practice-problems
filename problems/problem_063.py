# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    # split string into individual letters
    # convert ea letter to char code (map), add 1
    # convert back to alpha
    # join result, return
    split_string = [*string]
    num_string = list(map(lambda a: ord(a) + 1, split_string))
    # print(num_string)
    char_string = list(map(lambda a: chr(a), num_string))
    return ''.join(char_string)

print(shift_letters('alpha'))
