# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
#input = 2 numeric values: val1, val2
#if val1 == val2, return val1
    if value1 == value2:
        return value1
    else:
        return min(value1, value2)
print(minimum_value(4, 3))
#else return min(val1, val2)
#print return
