# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #create copy of word but in reverse
    rev_word_list = reversed(word)
    #if word_copy in word return true, else eturn false: must join word back together for comparison
    reversed_word = ''.join(rev_word_list)
    #print(reversed_word)
    return reversed_word == word

print(is_palindrome('racecar'))
